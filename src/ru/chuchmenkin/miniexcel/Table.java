package ru.chuchmenkin.miniexcel;

import java.lang.reflect.Array;

public class Table<T> {
	private T[][] cells;
	private int rowCount;
	private int columnCount;

	@SuppressWarnings("unchecked")
	public Table(int rowCount, int columnCount, Class<T> tClass) {
		if (rowCount < 1) {
			throw new IllegalArgumentException("Row count must be greater then 0");
		}
		this.rowCount = rowCount;
		if (columnCount < 1) {
			throw new IllegalArgumentException("Column count must be greater then 0");
		}
		this.columnCount = columnCount;
		cells = (T[][]) Array.newInstance(tClass, rowCount, columnCount);
	}

	public int getRowCount() {
		return rowCount;
	}

	public int getColumnCount() {
		return columnCount;
	}

	public void setRowFromArray(T[] rowData, int rowIndex) {
		checkupIndex(rowIndex, rowCount);
		for (int i = 0; i < columnCount; i++) {
			if (i < rowData.length) {
				cells[rowIndex][i] = rowData[i];
			}
			else {
				cells[rowIndex][i] = null;
			}
		}
	}

	
	// can index be used in the table, depends on what index is it
	private void checkupIndex(int index, int validationCount) {
		if (index > validationCount - 1 || index < 0) {
			throw new IndexOutOfBoundsException(
					"Index out of bounds. Actual value: " + index + ". Should be from 0 to " + validationCount);
		}
	}

	public void setCell(T cellData, int rowIndex, int columnIndex) {
		checkupIndex(rowIndex, rowCount);
		checkupIndex(columnIndex, columnCount);
		cells[rowIndex][columnIndex] = cellData;
	}

	public void setCell(T cellData, String reference) {
		if (reference == null) {
			throw new IllegalArgumentException("reference must not be null");
		}
		if (reference.length() > 2) {
			throw new IllegalArgumentException("Error in reference");
		}
		if (!Character.isLetter(reference.charAt(0)) || !Character.isDigit(reference.charAt(1))) {
			throw new IllegalArgumentException("Error in reference");
		}
		int columnIndex = convertLetterToIndex(reference.charAt(0));
		int rowIndex = Character.getNumericValue(reference.charAt(1));
		if (rowIndex > rowCount) {
			throw new IndexOutOfBoundsException("Reference row index out of bounds of table");
		}
		if (columnIndex > columnCount) {
			throw new IndexOutOfBoundsException("Reference column index out of bounds of table");
		}
		cells[rowIndex - 1][columnIndex - 1] = cellData;
	}

	public T getCell(int rowIndex, int columnIndex) {
		checkupIndex(rowIndex, rowCount);
		checkupIndex(columnIndex, columnCount);
		return cells[rowIndex][columnIndex];
	}

	private int convertLetterToIndex(char letter) {
		return Character.toLowerCase(letter) - 'a' + 1;
	}

	public T getCell(String reference) {
		if (reference.length() > 2) {
			throw new IllegalArgumentException("Error in reference");
		}
		if (!Character.isLetter(reference.charAt(0)) || !Character.isDigit(reference.charAt(1))) {
			throw new IllegalArgumentException("Error in reference");
		}
		int columnIndex = convertLetterToIndex(reference.charAt(0));
		int rowIndex = Character.getNumericValue(reference.charAt(1));
		if (rowIndex > rowCount) {
			throw new IndexOutOfBoundsException("Reference row index out of bounds of table");
		}
		if (columnIndex > columnCount) {
			throw new IndexOutOfBoundsException("Reference column index out of bounds of table");
		}
		return cells[rowIndex - 1][columnIndex - 1];
	}

	public int getMaxStringLengthInColumn(int columnIndex) {
		checkupIndex(columnIndex, columnCount);
		int maxLength = 0;
		for (int i = 0; i < rowCount; i++) {
			int length = cells[i][columnIndex].toString().length();
			if (length > maxLength) {
				maxLength = length;
			}
		}
		return maxLength;
	}

	public int[] getMaxStringLengthsInEachColumn() {
		int[] lengths = new int[columnCount];
		for (int j = 0; j < columnCount; j++) {
			lengths[j] = getMaxStringLengthInColumn(j);
		}
		return lengths;
	}

	public String printTable() {
		int[] maxLengthsInEachColumn = getMaxStringLengthsInEachColumn();
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < columnCount - 1; j++) {
				builder.append(cells[i][j].toString());

				// this code makes table look fancy 
				int emptyStringLength = maxLengthsInEachColumn[j] - cells[i][j].toString().length() + 1;
				builder.append(String.format("%" + emptyStringLength + "s", "\t"));
			}
			builder.append(cells[i][columnCount - 1] + "\n");
		}
		return builder.toString();
	}
}
