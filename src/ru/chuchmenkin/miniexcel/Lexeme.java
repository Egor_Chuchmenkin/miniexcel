package ru.chuchmenkin.miniexcel;

import java.util.Objects;

public class Lexeme {
	private String value;
	private int code;
	
	public Lexeme(String value, int code) {
		this.value = Objects.requireNonNull(value, "value must no be null");
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getCode() {
		return code;
	}
}
