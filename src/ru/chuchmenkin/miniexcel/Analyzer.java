package ru.chuchmenkin.miniexcel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import ru.chuchmenkin.miniexcel.ProcessedResult.Type;

@SuppressWarnings("serial")
public class Analyzer {

	class ScanLexemeException extends Exception {
		public ScanLexemeException(String message) {
			super(message);
		}
	}

	class RecursionException extends Exception {
		public RecursionException(String message) {
			super(message);
		}
	}

	class CellTypeException extends Exception {
		public CellTypeException(String message) {
			super(message);
		}
	}

	class GrammaticException extends Exception {
		public GrammaticException(String message) {
			super(message);
		}
	}

	private static final int POSITIVE_NUMBER_CODE = 0;
	private static final int CELL_REFERENCE_CODE = 1;

	private static final char FIRST_TEXT_CHARACTER = '\'';
	private static final char FIRST_EXPRESSION_CHARACTER = '=';
	private static final char SPACE = ' ';

	private static final String EMPTY_SOURCE_STRING = "#NoData";
	private static final String NOT_POSITIVE_NUMBER = "#NotPosNum";
	private static final String GRAMMATIC_ERROR = "#GramErr";
	private static final String NOT_VALID_REFERENCE = "#ImproperRef";
	private static final String INFINITE_REFERENCE_LOOP = "#InfLoop";
	private static final String ERROR_IN_CELL_BY_REFERENCE = "#ErrInCellByRef";
	private static final String UNRECKOGNIZED_TOKEN = "#TokenErr";
	private static final String ERROR_IN_REFERENCE = "#RefErr";
	private static final String MISSING_TERM = "#MissedTerm";
	private static final String UNDERFLOW_INT = "#Underflow";
	private static final String OVERFLOW_INT = "#Overflow";
	private static final String DIVISION_BY_ZERO = "#DivByZero";

	private static final Hashtable<Character, Integer> TOKENS = new Hashtable<Character, Integer>() {
		{
			put('+', 2);
			put('-', 3);
			put('*', 4);
			put('/', 5);
		}
	};

	private Table<ProcessedResult> processedTable;
	private Table<String> dataTable;

	// stores previous called cell references so it won't stuck in recursion
	private HashSet<String> calledCellReferences;

	public Analyzer(Table<String> dataTable) {
		this.dataTable = Objects.requireNonNull(dataTable, "dataTable must not be null");
		processedTable = new Table<>(dataTable.getRowCount(), dataTable.getColumnCount(), ProcessedResult.class);
		calledCellReferences = new HashSet<>();
	}

	private ProcessedResult analyzeTableCell(String tableCell) {
		if (tableCell == null) {
			return new ProcessedResult(Type.Error, EMPTY_SOURCE_STRING);
		}
		if (tableCell.length() == 0) {
			return new ProcessedResult(Type.Empty, "");
		} else {
			switch (tableCell.charAt(0)) {
			case FIRST_TEXT_CHARACTER:
				return new ProcessedResult(Type.Text, tableCell.substring(1));
			case FIRST_EXPRESSION_CHARACTER:
				return analyzeExpression(tableCell.substring(1));
			default:

				/*
				 * tableCell is not expression and not text, so it should be
				 * non-negative number. So try to parse it to UnsignedInt for
				 * sure. If it fails, set the error.
				 */
				try {
					Integer.parseUnsignedInt(tableCell);
				} catch (NumberFormatException exc) {
					return new ProcessedResult(Type.Error, NOT_POSITIVE_NUMBER);
				}
				return new ProcessedResult(Type.Number, tableCell);
			}
		}
	}

	private ProcessedResult analyzeExpression(String expression) {
		try {
			return new ProcessedResult(Type.Expression, Integer.toString(countExpression(scanExpression(expression))));
		} catch (Exception exc) {
			return new ProcessedResult(Type.Error, exc.getMessage());
		}
	}

	// counts result of operations on lexemes of expression 
	private int countExpression(List<Lexeme> lexemes)
			throws GrammaticException, IllegalArgumentException, RecursionException, CellTypeException {
		Iterator<Lexeme> lexemeIterator = lexemes.iterator();
		if (!lexemeIterator.hasNext()) {
			throw new GrammaticException(GRAMMATIC_ERROR);
		}
		Lexeme lexeme = lexemeIterator.next();

		/*
		 * first lexeme in expression should be term, so get value of it
		 */
		int result = getTermValue(lexeme);
		while (lexemeIterator.hasNext()) {
			result = processNextOperator(lexemeIterator, result);
		}
		return result;
	}

	private int getTermValue(Lexeme lexeme)
			throws GrammaticException, RecursionException, IllegalArgumentException, CellTypeException {
		switch (lexeme.getCode()) {
		case POSITIVE_NUMBER_CODE:
			return Integer.parseInt(lexeme.getValue());
		case CELL_REFERENCE_CODE:
			try {
				return getNumberByReference(lexeme.getValue());
			} catch (IllegalArgumentException | IndexOutOfBoundsException exc) {
				throw new IllegalArgumentException(NOT_VALID_REFERENCE);
			} catch (RecursionException exc) {
				throw new RecursionException(INFINITE_REFERENCE_LOOP);
			} catch (CellTypeException exc) {
				throw new CellTypeException(ERROR_IN_CELL_BY_REFERENCE);
			} finally {
				
				/*
				 *  if reference didn't removed due to exception,
				 *  clear reference calls
				 */
				calledCellReferences.clear();
			}
		default:
			throw new GrammaticException(GRAMMATIC_ERROR);
		}

	}

	private int processNextOperator(Iterator<Lexeme> iterator, int currentValue)
			throws GrammaticException, IllegalArgumentException, RecursionException, CellTypeException {
		Lexeme lexeme = iterator.next();
		if (TOKENS.contains(lexeme.getCode())) {
			char operator = lexeme.getValue().charAt(0);
			if (!iterator.hasNext()) {
				throw new GrammaticException(MISSING_TERM);
			}

			/*
			 * get results of operations in long, so we can know, when it will
			 * overflow or underflow int
			 */
			long nextTermValue = (long) getTermValue(iterator.next());
			long result = 0;
			switch (operator) {
			case '+':
				result = currentValue + nextTermValue;
				break;
			case '-':
				result = currentValue - nextTermValue;
				break;
			case '*':
				result = currentValue * nextTermValue;
				break;
			case '/':
				if (nextTermValue == 0) {
					throw new ArithmeticException(DIVISION_BY_ZERO);
				}
				result = currentValue / nextTermValue;
				break;
			default:
				throw new GrammaticException(GRAMMATIC_ERROR);
			}
			if (result < Integer.MIN_VALUE) {
				throw new ArithmeticException(UNDERFLOW_INT);
			} else if (result > Integer.MAX_VALUE) {
				throw new ArithmeticException(OVERFLOW_INT);
			}
			return (int) result;
		} else {
			throw new GrammaticException(GRAMMATIC_ERROR);
		}

	}

	// get number result of table cell by the reference
	private int getNumberByReference(String cellReference) throws RecursionException, CellTypeException {

		/*
		 * check reference, was it already called. If it is true, function will
		 * get in recursion, we should throw error
		 */
		if (!calledCellReferences.add(cellReference)) {
			throw new RecursionException("Recursion is detected in references");
		}
		ProcessedResult cell = null;
		try {

			// reference could be possibly misleading, and it will throw exception
			cell = processedTable.getCell(cellReference);
		} catch (IllegalArgumentException exc) {
			throw exc;
		}

		// if cell null, we didn't process it before
		if (cell == null) {
			cell = analyzeTableCell(dataTable.getCell(cellReference));
			processedTable.setCell(cell, cellReference);
		}

		/*
		 * cell by reference was processed without exceptions, it can be removed
		 * from list of called references
		 */
		calledCellReferences.remove(cellReference);
		switch (cell.getType()) {
		case Expression:
			return Integer.parseInt(cell.getResult());
		case Number:
			return Integer.parseUnsignedInt(cell.getResult());
		default:
			throw new CellTypeException("Can't get number from cell with " + cell.getType() + " type.");
		}
	}

	private boolean isEnglishLetter(char ch) {
		return ((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'));
	}

	// returns list of lexemes of expression
	private List<Lexeme> scanExpression(String expression) throws ScanLexemeException {
		List<Lexeme> lexemes = new ArrayList<Lexeme>();
		char[] charData = expression.toCharArray();
		int length = expression.length();
		int pointer = 0;
		while (pointer < length) { // loop through all characters in expression
			if (charData[pointer] == SPACE) { // skip spaces
				pointer++;
			} else if (TOKENS.containsKey(charData[pointer])) { // get token value and code
				lexemes.add(new Lexeme(String.valueOf(charData[pointer]), TOKENS.get(charData[pointer])));
				pointer++;
			} else {
				Lexeme lexeme = null;
				try {

					// if current character - digit, get number lexeme
					if (Character.isDigit(charData[pointer])) {
						lexeme = getPositiveNumberLexemeFromString(expression, pointer);
					}

					// if current character - letter, get cell reference lexeme
					else if (isEnglishLetter(charData[pointer])) {
						lexeme = getCellReferenceLexemeFromString(expression, pointer);
					} else {
						throw new ScanLexemeException(UNRECKOGNIZED_TOKEN);
					}
				} catch (ScanLexemeException exception) {
					throw exception;
				}
				pointer += lexeme.getValue().length();
				lexemes.add(lexeme);
			}
		}
		return lexemes;
	}

	private Lexeme getPositiveNumberLexemeFromString(String sourceString, int startIndexOfNumber)
			throws ScanLexemeException {
		char[] charData = sourceString.toCharArray();
		int length = sourceString.length();
		int pointer = startIndexOfNumber;
		while (pointer < length && Character.isDigit(charData[pointer])) {
			pointer++;
		}
		if (pointer < length) {
			if (charData[pointer] == SPACE || TOKENS.containsKey(charData[pointer])) {
				return new Lexeme(sourceString.substring(startIndexOfNumber, pointer), POSITIVE_NUMBER_CODE);
			} else {
				if (Character.isLetter(charData[pointer])) {
					throw new ScanLexemeException(NOT_POSITIVE_NUMBER);
				} else {
					throw new ScanLexemeException(UNRECKOGNIZED_TOKEN);
				}
			}
		} else {
			return new Lexeme(sourceString.substring(startIndexOfNumber, pointer), POSITIVE_NUMBER_CODE);
		}
	}

	private Lexeme getCellReferenceLexemeFromString(String sourceString, int startIndexOfReference)
			throws ScanLexemeException {
		char[] charData = sourceString.toCharArray();
		int length = sourceString.length();
		int pointer = startIndexOfReference;
		if (pointer < length) {
			pointer++;
			if (Character.isDigit(charData[pointer])) {
				pointer++;
				if (pointer < length) {
					if (charData[pointer] == SPACE || TOKENS.containsKey(charData[pointer])) {
						return new Lexeme(sourceString.substring(startIndexOfReference, pointer), CELL_REFERENCE_CODE);
					} else {
						if (Character.isLetterOrDigit(charData[pointer])) {
							throw new ScanLexemeException(ERROR_IN_REFERENCE);
						} else {
							throw new ScanLexemeException(UNRECKOGNIZED_TOKEN);
						}
					}
				} else {
					return new Lexeme(sourceString.substring(startIndexOfReference, pointer), CELL_REFERENCE_CODE);
				}
			}
		}
		throw new ScanLexemeException(ERROR_IN_REFERENCE);
	}

	public Table<String> analyzeTable() {
		int rowCount = dataTable.getRowCount();
		int columnCount = dataTable.getColumnCount();
		Table<String> resultTable = new Table<>(rowCount, columnCount, String.class);
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < columnCount; j++) {
				ProcessedResult processedTableCell = processedTable.getCell(i, j);
				if (processedTableCell == null) {
					processedTableCell = analyzeTableCell(dataTable.getCell(i, j));

					// check-up, wasn't the result placed during processing, to not override error
					if (processedTable.getCell(i, j) == null) {
						processedTable.setCell(processedTableCell, i, j);
					} else {
						processedTableCell = processedTable.getCell(i, j);
					}
				}
				resultTable.setCell(processedTableCell.getResult(), i, j);
			}
		}
		return resultTable;
	}
}