package ru.chuchmenkin.miniexcel;

import java.util.Scanner;

public class MiniExcel {
	public static void main(String[] args) {
//		//		Test examples 
//		//		To launch tests, uncomment this block
//		Table<String> exampleTable = new Table<>(3, 4, String.class);
//		exampleTable.setRowFromArray(new String[] { "12", "=c2", "3", "'sample" }, 0);
//		exampleTable.setRowFromArray(new String[] { "=a1+b1*c1/5", "=A2*b1", "=b3-c3", "" }, 1);
//		exampleTable.setRowFromArray(new String[] { "'Very long test string", "=4-3", "5", "=c3+c3" }, 2);
//		Analyzer exampleAnalyzer = new Analyzer(exampleTable);
//		System.out.print(exampleAnalyzer.analyzeTable().printTable());
//
//		exampleTable = new Table<>(5, 5, String.class);
//		exampleTable.setRowFromArray(new String[] { "=a1", "=c1", "=b1", "=dd", "=1-2" }, 0);
//		exampleTable.setRowFromArray(new String[] { "1a", "Test", "2147483647", "=a44", "=�2"}, 1);
//		exampleTable.setRowFromArray(new String[] { "=4'3", "=4 3", "=c2+2", "=c2*e1-100" }, 2);
//		exampleTable.setRowFromArray(new String[] { "=c4", "=a4", "=b4", "=a9" }, 3);
//		exampleAnalyzer = new Analyzer(exampleTable);
//		System.out.print(exampleAnalyzer.analyzeTable().printTable());
		Scanner scanner = new Scanner(System.in);
		String splitter = "\t";
		String[] bounds = null;
		boolean inputValid = false;
		int rows = 0;
		int columns = 0;
		while (!inputValid) {
			try {
				bounds = scanner.nextLine().split(splitter);
				rows = Integer.parseInt(bounds[0]);
				columns = Integer.parseInt(bounds[1]);
				inputValid = true;
			} catch (Exception exc) {

				/*
				 * ignore exceptions of int parsing, just let user to try one
				 * more time
				 */
			}
		}
		Table<String> dataTable = new Table<>(rows, columns, String.class);
		for (int i = 0; i < rows; i++) {
			dataTable.setRowFromArray(scanner.nextLine().split(splitter), i);
		}
		Analyzer analyzer = new Analyzer(dataTable);
		System.out.print(analyzer.analyzeTable().printTable());
		scanner.close();
	}
}
