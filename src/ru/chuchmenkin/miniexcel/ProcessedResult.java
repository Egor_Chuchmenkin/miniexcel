package ru.chuchmenkin.miniexcel;

import java.util.Objects;

public class ProcessedResult {
	public static enum Type {
		Expression, Number, Empty, Text, Error
	}

	private Type type;
	private String result;

	public Type getType() {
		return type;
	}

	public String getResult() {
		return result;
	}

	public ProcessedResult(Type type, String result) {
		this.type = Objects.requireNonNull(type, "type must not be null");
		this.result = Objects.requireNonNull(result, "result must not be null");
	}

	@Override
	public String toString() {
		return result;
	}
}
